function logOut() {
    localStorage.setItem('panelUsername', '');
    localStorage.setItem('panelPassword', '');
    location.reload();
}

window.onblur = function() {
    window.blurred = true;
};
window.onfocus = function() {
    window.blurred = false;
};

function addCurrentUser() {
    var username = localStorage.getItem('panelUsername');
    var password = localStorage.getItem('panelPassword');

    $.ajax({
        type: "POST",
        url: "php/AddCurrentUser.php",
        data: {
            'USERNAME': username,
            'PASSWORD': password
        },
        success: function(data) {
            if (data == 'invalid') {
                alert("Invalid Credentials!");
                localStorage.setItem('panelPassword', password);
            } else if (data == 'correct') {

            }
        }
    });
}

function test() {
    // $.ajax({
    //     type: "POST",
    //     url: "http://192.223.30.171/bans/php/GetUser.php",
    //     data: {
    //         'IP': "62.61.130.34"
    //     },
    //     success: function(data) {
    //         console.log(data);
    //         var toSplit = data;
    //         console.log(toSplit);
    //         var parts = toSplit.split('@');
    //
    //         var divided = [];
    //
    //         var phpIPS = new Array();
    //
    //         for (var x = 0; x < parts.length; x++) {
    //             var divide = parts[x];
    //             divided = divide.split('~');
    //             phpIPS.push(divided[6]);
    //         }
    //
    //         var length = phpIPS.length;
    //
    //         var username = document.createElement("span");
    //         username.innerHTML = "test<br>" + phpIPS[length];
    //         username.setAttribute("style", "color:lime-green");
    //         $(username).appendTo($(postUsernames[y]).parent());
    //         index = index + 1;
    //         console.log(phpIPS[length]);
    //     }
    // });
    $.ajax({
        type: "POST",
        url: "http://192.223.30.171/bans/php/GetUser.php",
        data: {
            'IP': "62.61.130.34"
        },
        success: function(data) {
            // console.log(IPS[index]);
            console.log(data);
            var toSplit = data;
            var parts = toSplit.split('@');

            var divided = [];

            var phpIPS = new Array();

            for (var x = 0; x < parts.length; x++) {
                var divide = parts[x];
                divided = divide.split('~');
                phpIPS.push(divided[6]);
            }

            var length = phpIPS.length;

            var username = document.createElement("span");
            username.innerHTML = "test<br>" + phpIPS[length];
            username.setAttribute("style", "color:lime-green");
            $(username).appendTo($(postUsernames[y]).parent());
            index = index + 1;
            console.log(phpIPS[length]);
        }
    });
}

// function getCurrentUsers() {
//     var password = localStorage.getItem('panelPassword');
//
//     $.ajax({
//         type: "POST",
//         url: "php/GetCurrentUsers.php",
//         data: {
//             'PASSWORD': password
//         },
//         success: function(data) {
//             if (data == 'invalid') {
//                 alert("Invalid Credentials!");
//                 localStorage.setItem('panelPassword', password);
//             } else {
//                 $('.current-user').remove();
//
//                 var users = new Array();
//                 users = data.split('~');
//                 var uniqueUsers = new Array();
//
//                 uniqueUsers = users.filter(function (item, index) {
//                     return users.indexOf(item) == index;
//                 });
//
//                 for (var x = 0; x < uniqueUsers.length - 1; x++) {
//                     var user = document.createElement('span');
//                     user.setAttribute('class', 'current-user');
//                     if (x == 0) {
//                         user.innerHTML = uniqueUsers[x];
//                     } else {
//                         user.innerHTML = ", " + uniqueUsers[x];
//                     }
//                     document.getElementById('users').appendChild(user);
//                 }
//                 $('.current-user').each(function() {
//                    $(this).html($(this).html().replace(',', '<span style="color: white;">,</span>'))
//                 });
//             }
//         }
//     });
// }

function wipeCurrentUsers() {
    var password = localStorage.getItem('panelPassword');

    $.ajax({
       type: "POST",
        url: 'php/WipeCurrentUsers.php',
        data: {
            'PASSWORD': password
        },
        success: function (data) {
            if (data == 'incorrect') {
                alert("Invalid Credentials!");
            } else {
            }
        }
    });
}

function testing() {
    var testButton = document.createElement('div');
    testButton.setAttribute('class', 'ui-button');
    testButton.setAttribute('onclick', 'example()');
    testButton.setAttribute('id', 'test-button');
    testButton.setAttribute('data-guid', '897346y0rwhe79qun403qop5ythr');
    testButton.setAttribute('data-ip', '127.0.0.1');
    testButton.innerHTML = "Test 2";

    document.getElementById('menu').appendChild(testButton);
}

function example() {
    console.log($('#test-button').data('guid'));
}
