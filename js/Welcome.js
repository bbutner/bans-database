function banCount() {
    var username = localStorage.getItem('panelUsername');
    var password = localStorage.getItem('panelPassword');

    $.ajax({
        type: "POST",
        url: "php/BanCount.php",
        data: {
            'USERNAME': username,
            'PASSWORD': password
        },
        success: function(data) {
            if (data == 'invalid') {
                alert("Invalid Credentials!");
            } else {
                $('#ban-count').text(data);
            }
        }
    })
}

function recentBan() {
    var username = localStorage.getItem('panelUsername');
    var password = localStorage.getItem('panelPassword');

    $.ajax({
        type: "POST",
        url: "php/RecentBan.php",
        data: {
            'USERNAME': username,
            'PASSWORD': password
        },
        success: function(data) {
            if (data == 'invalid') {
                alert("Invalid Credentials!");
            } else {
                var toSplit = data;
                var parts = new Array();
                parts = toSplit.split('~');

                $('#recent-ban').text(parts[3]);
            }
        }
    })
}

function loadWelcome() {
    var username = localStorage.getItem('panelUsername');
    $('.welcome').css('visibility', 'visible');

    $('#current-user').text(username);
    $('.welcome-content').animate({opacity: 1}, 1000);
}

function bannedToday() {
    var password = localStorage.getItem('panelPassword');

    $.ajax({
        type: "POST",
        url: "php/Today.php",
        data: {
            'PASSWORD': password
        },
        success: function (data) {
            $('#banned-today').text(data);
        }
    })
}

function getTotalBanCount() {
    var password = localStorage.getItem('panelPassword');

    $.ajax({
        type: "POST",
        url: "php/TotalBanCount.php",
        data: {
            'PASSWORD': password
        },
        success: function (data) {
            if (data == 'incorrect') {
                alert("Invalid Credentials");
            } else {
                $('#total-bans').text(data);
            }
        }
    });
}