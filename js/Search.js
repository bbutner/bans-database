var bansVisible = "false";
var bansListVisible = "false";

$('#search-bans').ready(function() {
    $('#search-button').on('click', function (event) {
        searchDB();
    })
});

$(document).ready(function() {
    $(document).on('keypress', function (e) {
        if(e.which == 13) {
            var query_admin_username = document.getElementById('search-admin').value;
            var query_guid = document.getElementById('search-guid').value;
            var query_ip = document.getElementById('search-ip').value;
            var query_ban_reason = document.getElementById('search-ban-reason').value;
            var query_username = document.getElementById('search-username').value;

            if (query_admin_username == '' &&
                query_guid == '' &&
                query_ip == '' &&
                query_ban_reason == '' &&
                query_username == '') {

            } else {
                searchDB();
            }
        }
    });
});

$('#clear-button').ready(function() {
    $('#clear-button').on('click', function (event) {
        document.getElementById('search-username').value = '';
        document.getElementById('search-guid').value = '';
        document.getElementById('search-ip').value = '';
        document.getElementById('search-ban-reason').value = '';
        document.getElementById('search-admin').value = '';
        displayContent("button_search");
    })
});

function searchDB() {
    var username = localStorage.getItem('panelUsername');
    var password = localStorage.getItem('panelPassword');

    $('.ui-button-bans').remove();

    $.ajax({
        type: "POST",
        url: "php/Authorize.php",
        data: {
            'USERNAME': username,
            'PASSWORD': password
        },
        success: function (data) {
            if (data == 'incorrect') {
                window.location = 'http://192.223.30.171/bans';
            } else if (data == 'correct') {
                console.log(data);
                var adminusername = localStorage.getItem('panelUsername');
                var query_admin_username = document.getElementById('search-admin').value;
                var password = localStorage.getItem('panelPassword');
                var query_guid = document.getElementById('search-guid').value;
                var query_ip = document.getElementById('search-ip').value;
                var query_ban_reason = document.getElementById('search-ban-reason').value;
                var query_username = document.getElementById('search-username').value;

                var queryTypes = [query_guid, query_ip, query_username, query_ban_reason, query_admin_username];

                if (adminusername == '') {
                    alert("Please enter your username at the top!");
                    return;
                }

                if (query_guid == '' &&
                    query_ip == '' &&
                    query_username == '' &&
                    query_ban_reason == '' &&
                    query_admin_username == '') {
                    alert("Please enter a search term!");
                    return;
                }

                //Check beginning for spaces
                for (var x = 0; x < queryTypes.length; x++) {
                    var spaces = false;

                    if (queryTypes[x].charAt(0) == ' ') {
                        spaces = true;
                    }

                    while (spaces) {
                        queryTypes[x] = queryTypes[x].substr(1);

                        if (queryTypes[x].charAt(0) == ' ') {
                        } else {
                            break;
                        }
                    }
                }

                //Check the end for spaces
                for (var x = 0; x < queryTypes.length; x++) {
                    var spaces = false;

                    var lastChar = queryTypes[x].length - 1;

                    if (queryTypes[x].charAt(lastChar) == ' ') {
                        spaces = true;
                    }

                    while (spaces) {
                        queryTypes[x] = queryTypes[x].substr(0, queryTypes[x].length - 1)
                        var newLength = lastChar - 1;

                        if (queryTypes[x].charAt(newLength) == ' ') {
                        } else {
                            break;
                        }
                    }
                }

                //DATE, DBUSERNAME, GUID, IP, USERNAME, BAN_REASON
                var typesToBeSent = [];

                if (queryTypes[4] != '') {
                    typesToBeSent.push("query_admin_username");
                }

                if (queryTypes[0] != '') {
                    typesToBeSent.push("query_guid");
                }

                if (queryTypes[1] != '') {
                    typesToBeSent.push("query_ip");
                }

                if (queryTypes[2] != '') {
                    typesToBeSent.push("query_username");
                }

                if (queryTypes[3] != '') {
                    typesToBeSent.push("query_ban_reason");
                }

                var sentData = JSON.stringify(typesToBeSent);

                $.ajax({
                    type: "POST",
                    url: "php/Search.php",
                    data: {
                        'ACCESS_USERNAME': adminusername,
                        'ADMIN_USERNAME': queryTypes[4],
                        'PASSWORD': password,
                        'GUID': queryTypes[0],
                        'IP': queryTypes[1],
                        'USERNAME': queryTypes[2],
                        'BAN_REASON': queryTypes[3],
                        'QUERY_TYPE': sentData
                    },
                    success: function (data) {
                        $('.search-wrapper').css('border-bottom', '2px solid #009688');
                        $('.search-results').css('visibility', 'visible');
                        addBansToDropdown(data);
                    }
                });
            }
        }
    });
}

function addBansToDropdown(bans) {
    var toSplit = bans;
    var parts = new Array();
    parts = toSplit.split("@");

    var divided = [];

    var DATE = new Array();
    var ADMIN_NAME = new Array();
    var USERNAME = new Array();
    var GUID = new Array();
    var IP = new Array();
    var BAN_REASON = new Array();

    for (var x = 0; x < parts.length - 1; x++) {
        var toBeDivided = parts[x] + '';
        divided = toBeDivided.split('~');
        DATE.push(divided[1]);
        ADMIN_NAME.push(divided[2]);
        USERNAME.push(divided[3]);
        GUID.push(divided[4]);
        IP.push(divided[5]);
        BAN_REASON.push(divided[6]);
    }

    var uniquePlayers = USERNAME.filter(function (item, index) {
        return USERNAME.indexOf(item) == index;
    });

    var uniqueGUIDs = GUID.filter(function (item, index) {
       return GUID.indexOf(item) == index;
    });

    for (var x = 0; x < uniquePlayers.length; x++) {
        var banButton = document.createElement("div");
        banButton.setAttribute('data-username', uniquePlayers[x]);
        banButton.setAttribute('data-guid', uniqueGUIDs[x]);
        banButton.innerHTML = uniquePlayers[x];
        banButton.setAttribute('class', 'ui-button-bans');
        banButton.setAttribute('onclick', 'displayBans(this)\;activeButton(this)');
        document.getElementById('bans').appendChild(banButton);
        $('.dropdown-bans').css('border-bottom', '2px solid #009688');
    }

    $('#player-count').text($('.ui-button-bans').length);
}

function activeButton(e) {
    $('.ui-button-bans').css('background-color', '#202329');
    $(e).css('background-color', '#1C1F24');
}

function activeReason(e) {
    $('.ui-button-reasons').css('background-color', '#202329');
    $(e).css('background-color', '#1C1F24');
}

function displayBans(idName) {
    var username = $(idName).data('username');

    var password = localStorage.getItem('panelPassword');

    $('.ui-button-reasons').remove();
    $('.results').css('visibility', 'hidden');

    $.ajax({
        type: "POST",
        url: "php/GetBanReasons.php",
        data: {
            'PASSWORD': password,
            'USERNAME': username
        },
        success: function(data) {
            if (data == 'invalid') {
                alert("Invalid Credentials!");
            } else {
                var toSplit = data;
                var parts = toSplit.split('@');

                var divided = [];

                var BAN_REASONS = new Array();
                var USERNAMES = new Array();

                for (var x = 0; x < parts.length - 1; x++) {
                    var toBeDivided = parts[x];
                    divided = toBeDivided.split('~');
                    BAN_REASONS.push(divided[6]);
                    USERNAMES.push(divided[3])
                }

                for (var x = 0; x < BAN_REASONS.length; x++) {
                    var reason = BAN_REASONS[x];
                    var replaced = reason.replace(USERNAMES[x] + " - ", "");

                    var reasonButton = document.createElement("div");
                    reasonButton.setAttribute('data-username', USERNAMES[x]);
                    reasonButton.setAttribute('data-ban-reason', BAN_REASONS[x]);
                    reasonButton.setAttribute('class', 'ui-button-reasons');
                    reasonButton.innerHTML = replaced;
                    reasonButton.setAttribute('onclick', 'getBanInfo(this)\;activeReason(this)');
                    document.getElementById('bans-reasons').appendChild(reasonButton);
                }

                $('#banCount').text($('.ui-button-reasons').length);
                $('.dropdown-bans-reasons').css('border-bottom', '2px solid #009688');
                $('.bans-reasons-dropdown').css('visibility', 'visible');
            }
        }
    });
}

function getBanInfo(playerInfo) {
    var password = localStorage.getItem('panelPassword');

    var username = $(playerInfo).data('username');
    var ban_reason = $(playerInfo).data('ban-reason');

    $.ajax({
        type: "POST",
        url: "php/BanInfo.php",
        data: {
            'PASSWORD': password,
            'USERNAME': username,
            'BAN_REASON': ban_reason
        },
        success: function(data) {
            if (data == 'invalid') {
                alert("Invalid Credentials!");
            } else {
                var toSplit = data;
                var parts = new Array();
                var parts = toSplit.split('~');

                $('#result-username').text(parts[3]);
                $('#result-admin').text(parts[2]);
                $('#result-guid').text(parts[4]);
                $('#result-ip').text(parts[5]);
                $('#result-ban-reason').text(parts[6]);
                $('#result-date').text(parts[1]);

                $('.results').css('visibility', 'visible');
            }
        }
    });
}

function deleteBan() {
    var username = $('#result-username').text();
    var guid = $('#result-guid').text();
    var ip = $('#result-ip').text();
    var banReason = $('#result-ban-reason').text();
    var banningAdmin = $('#result-admin').text();
    var password = localStorage.getItem('panelPassword');

    if (username == "" ||
    guid == "" ||
    ip == "" ||
    banReason == "" ||
    banningAdmin == "") {
        return;
    } else {
        if (confirm("Are you sure?") == true) {
            $.ajax({
                type: "POST",
                url: "php/Delete.php",
                data: {
                    'PASSWORD': password,
                    'USERNAME': username,
                    'GUID': guid,
                    'IP': ip,
                    'BAN_REASON': banReason,
                    'ADMIN': banningAdmin
                },
                success: function(data) {
                    if (data == 'invalid') {
                        alert("Invalid Credentials!");
                    } else {
                        alert("Ban Deleted.");
                        $('.results').css('visibility', 'hidden');
                        $('.bans-reasons-dropdown').css('visibility', 'hidden');
                        searchDB();
                    }
                }
            });
        }
    }
}
