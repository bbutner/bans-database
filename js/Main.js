$('.button').ready(function() {
    $('.button').on('click', function (event) {
        $('#time-button').css('visibility', 'hidden');
        $('.button').removeClass('active');
        $('.button').css('background-color', '#282c34');
        $target = $(event.target);
        $target.addClass('active');
        $(this).css('background-color', '#202329');
        displayContent(event.target.id);
    })
});

function authorizeUser() {
    var username = localStorage.getItem('panelUsername');
    var password = localStorage.getItem('panelPassword');

    $.ajax({
        type: "POST",
        url: "php/Authorize.php",
        data: {
            'USERNAME': username,
            'PASSWORD': password
        },
        success: function (data) {
            if (data == 'incorrect') {
                $('.login-ui').css('visibility', 'visible');
            } else if (data == 'correct') {
                banCount();
                recentBan();
                bannedToday();
                getTotalBanCount();
                $('#button_home').addClass('active');
                $('#button_home').css('background-color', '#202329');
                setTimeout(function() {loadWelcome();}, 100);

                addCurrentUser();
                // getCurrentUsers();
                wipeCurrentUsers();
            }
        }
    });
}

function logIn() {
    var username = $('#username-input').val();
    var password = $('#password-input').val();

    if (username == '' || password == '') {
        alert("Please enter a username and password!");
        return;
    }

    $.ajax({
        type: "POST",
        url: "php/Login.php",
        data: {
            'USERNAME': username,
            'PASSWORD': password
        },
        success: function(data) {
            if (data == 'invalid') {
                alert("Invalid Credentials!");
                localStorage.setItem('panelPassword', password);
            } else if (data == 'correct') {
                localStorage.setItem('panelPassword', password);
                localStorage.setItem('panelUsername', username);
                $('.login-ui').css('visibility', 'hidden');
                authorizeUser();
            }
        }
    });
}

function setBodyPadding() {
    var sideBarWidth = document.getElementById('sidebar').offsetWidth;

    $('body').css('padding-left', sideBarWidth);
}

function displayContent(type) {
    var menuHeight = document.getElementById('menu').offsetHeight;
    var addHeight = document.getElementById('add-ban').offsetHeight;
    var searchHeight = document.getElementById('search-bans').offsetHeight;
    $('.welcome').css('visibility', 'hidden');
    $('.welcome-content').css('opacity', '0');
    $('.menu').css('visibility', 'hidden');
    $('.menu').css('position', 'fixed');
    $('.add-ban').css('visibility', 'hidden');
    $('.add-ban').css('position', 'fixed');
    $('.search-bans').css('visibility', 'hidden');
    $('.search-bans').css('position', 'fixed');
    $('.search-results').css('visibility', 'hidden');
    $('.bans-reasons-dropdown').css('visibility', 'hidden');
    $('.add-info').css('visibility', 'hidden');
    $('.results').css('visibility', 'hidden');
    $('#guid-count').css('visibility', 'hidden');
    $('.dropdown-reasons').css('visibility', 'hidden');
    $('.dropdown-time').css('visibility', 'hidden');
    $('#ban-output').css('visibility', 'hidden');
    $('.ui-button-time').css('visibility', 'hidden');
    document.getElementById('add-username').value = "";
    document.getElementById('add-guid').value = "";
    document.getElementById('add-ip').value = "";
    document.getElementById('add-ban-reason').value = "";

    var username = localStorage.getItem('panelUsername');
    var password = localStorage.getItem('panelPassword');

    $.ajax({
        type: "POST",
        url: "php/Authorize.php",
        data: {
            'USERNAME': username,
            'PASSWORD': password
        },
        success: function (data) {
            if (data == 'incorrect') {
                $('.login-ui').css('visibility', 'visible');
            } else if (data == 'correct') {
                if (type == "button_menu") {
                    $('.menu').css('visibility', 'visible');
                    $('.menu').css('position', 'static');
                }

                if (type == "button_add") {
                    $('.add-ban').css('visibility', 'visible');
                    $('.add-ban').css('position', 'static');
                    showReasons();
                }

                if (type == "button_search") {
                    $('.search-bans').css('visibility', 'visible');
                    $('.search-bans').css('position', 'static');
                }

                if (type == "button_home") {
                    $('.welcome').css('visibility', 'visible');
                    authorizeUser();
                }
            }
        }
    });
}