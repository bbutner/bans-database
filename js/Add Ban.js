$(document).ready(function() {
    $('.ui-button-listreasons').on('click', function (event) {
        $('.ui-button-listreasons').css('background-color', '#202329');
        $(this).css('background-color', '#1C1F24');
        banReasonCreation(event.target.innerHTML);
        showTimes();
    });

    $('.ui-button-time').on('click', function (event) {
        $('.ui-button-time').css('background-color', '#202329');
        if (document.getElementById('add-ban-reason').value == "") {
            alert("Please choose a ban reason!");
        } else {
            $(this).css('background-color', '#1C1F24');
            banTimeCreation(event.target.innerHTML);
        }
    });
});

function clearField(field) {
    switch (field) {
        case "username":
            document.getElementById('add-username').value = "";
            return;
        case "guid":
            document.getElementById('add-guid').value = "";
            return;
        case "ip":
            document.getElementById('add-ip').value = "";
            return;
        case "banReason":
            document.getElementById('add-ban-reason').value = "";
            return;
    }
}

function showReasons() {
    $('.dropdown-reasons').css('visibility', 'visible');
    $('#reasons-caret').html('&#9660;');
    reasonsVisible = "true";
    return;
}

function showTimes() {
    $('.dropdown-time').css('visibility', 'visible');
    $('.ui-button-time').css('visibility', 'visible');
    $('#time-caret').html('&#9660;');
    timesVisible = "true";
}

var banReason;
var reasonChosen = "false";
var banTime;
var customTime = "false";

function banReasonCreation(reason) {
    if (reason == "Custom") {
        $('#text-ban-reason').css('opacity', '1');
        $('#add-ban-reason').css('opacity', '1');
        $('#time-button').css('visibility', 'visible');
        document.getElementById('add-ban-reason').value = "";
        customTime = "true";
    } else {
        banReason = reason;
        reasonChosen = "true";
        $('#time-button').css('visibility', 'visible');
        document.getElementById('add-ban-reason').value = reason;
    }
}

function banTimeCreation(time) {
    if (time == "Custom") {
        document.getElementById('add-ban-reason').value += " - ";
        $('#text-ban-reason').css('opacity', '1');
        $('#add-ban-reason').css('opacity', '1');
    } else {
        banTime = time;
        var banText = document.getElementById('add-ban-reason').value;
        banText = banText.replace(" - 12h", "");
        banText = banText.replace(" - 24h", "");
        banText = banText.replace(" - 48h", "");
        banText = banText.replace(" - 72h", "");
        banText = banText.replace(" - Perm", "");
        document.getElementById('add-ban-reason').value = banText + " - " + banTime;
    }
}

function getBan() {
    return document.getElementById('add-ban-reason').value;
}


function addBan() {
    var ban = getBan();

    if (ban == "" || ban == null || ban.indexOf("@") != -1 || ban.indexOf("~") != -1) {
        alert("Please enter a valid ban reason!");
        return;
    }

    var password = localStorage.getItem('panelPassword');

    $.ajax({
        type: "POST",
        url: "php/Authorize.php",
        data: {
            'PASSWORD': password
        },
        success: function (data) {
            if (data == 'incorrect') {
                window.location = 'http://192.223.30.171/bans';
            } else if (data == 'correct') {
                var admin_username = localStorage.getItem('panelUsername');
                var password = localStorage.getItem('panelPassword');
                var guid = document.getElementById('add-guid').value;
                var ip = document.getElementById('add-ip').value;
                var username = document.getElementById('add-username').value;

                var allowed = "true";

                if (guid == '' ||
                    guid.includes("@") ||
                    guid.includes("~")) {
                    $('#add-guid').removeClass('errorFlash');
                    setTimeout(function () {
                        $('#add-guid').addClass('errorFlash');
                    }, 1);
                    allowed = "false";
                }
                if (ip == '' ||
                    ip.includes("@") ||
                    ip.includes("~")) {
                    $('#add-ip').removeClass('errorFlash');
                    setTimeout(function () {
                        $('#add-ip').addClass('errorFlash');
                    }, 1);
                    allowed = "false";
                }
                if (username == '' ||
                    username.includes("@") ||
                    username.includes("~")) {
                    $('#add-username').removeClass('errorFlash');
                    setTimeout(function () {
                        $('#add-username').addClass('errorFlash');
                    }, 1);
                    allowed = "false";
                }
                if (ban == '' ||
                    ban.includes("@") ||
                    ban.includes("~")) {
                    $('#add-ban-reason').removeClass('errorFlash');
                    setTimeout(function () {
                        $('#add-ban-reason').addClass('errorFlash');
                    }, 1);
                    allowed = "false";
                }

                if (allowed == "false") {
                    $('.add-info').css('color', '#c21d1d');
                    $('#add-info').text("Please fill in all fields!");
                    $('.add-info').css('visibility', 'visible');
                    return;
                } else {
                    $('.add-info').css('color', '#009688');
                    $('#add-info').text("Ban successfully added: ");
                    $('#ban-output').text(username + " - " + ban);
                    $('#ban-output').css('visibility', 'visible');
                    $('.add-info').css('visibility', 'visible');
                }

                var banArgs = [admin_username, password, guid, ip, username, ban];

                //Check beginning for spaces
                for (var x = 0; x < banArgs.length; x++) {
                    var spaces = false;

                    if (banArgs[x].charAt(0) == ' ') {
                        spaces = true;
                    }

                    while (spaces) {
                        banArgs[x] = banArgs[x].substr(1);

                        if (banArgs[x].charAt(0) == ' ') {
                        } else {
                            break;
                        }
                    }
                }

                //Check end for spaces
                for (var x = 0; x < banArgs.length; x++) {
                    var spaces = false;

                    var lastChar = banArgs[x].length - 1;

                    if (banArgs[x].charAt(lastChar) == ' ') {
                        spaces = true;
                    }

                    while (spaces) {
                        banArgs[x] = banArgs[x].substr(0, banArgs[x].length - 1);
                        var newLength = banArgs[x].length - 1;

                        if (banArgs[x].charAt(newLength) == ' ') {
                        } else {
                            break;
                        }
                    }
                }

                $.ajax({
                    type: "POST",
                    url: "php/Add.php",
                    data: {
                        'ADMIN_USERNAME': banArgs[0],
                        'PASSWORD': banArgs[1],
                        'GUID': banArgs[2],
                        'IP': banArgs[3],
                        'USERNAME': banArgs[4],
                        'BAN_REASON': banArgs[5]
                    },
                    success: function (data) {
                        document.getElementById('add-guid').value = '';
                        document.getElementById('add-ip').value = '';
                        document.getElementById('add-username').value = '';
                        document.getElementById('add-ban-reason').value = '';
                        $('#text-ban-reason').css('opacity', '0');
                        $('#add-ban-reason').css('opacity', '0');
                        $('#time-button').css('visibility', 'hidden');
                        $('.ui-button-time').css('visibility', 'hidden');
                        banReason = "";
                        banTime = "";
                        $('.ui-button-listreasons').css('color', 'white');
                        showReasons();
                    }
                });

            }
        }
    });
}

function guidCount() {
    var guid = $('#add-guid').val();
    var password = localStorage.getItem('panelPassword');

    if (guid == '' ||
        guid == null) {
        alert("Please enter a GUID to search!");
    } else {
        $.ajax({
            type: "POST",
            url: "php/GuidCount.php",
            data: {
                'GUID': guid,
                'PASSWORD': password
            },
            success: function (data) {
                console.log(data);
                $('#guid-count').css('visibility', 'visible');
                document.getElementById('guid-count').innerHTML = "" + data + " Ban(s)";
            }
        });
    }
}

function searchGUID() {
    var guid = $('#add-guid').val();

    if (guid == '' ||
    guid == null) {
        alert("Please enter a GUID to search!");
    } else {
        document.getElementById('search-guid').value = '';
        document.getElementById('search-ip').value = '';
        document.getElementById('search-username').value = '';
        document.getElementById('search-ban-reason').value = '';
        document.getElementById('search-admin').value = '';
        document.getElementById('search-guid').value = guid;
        displayContent("button_search");
        searchDB();
    }
}