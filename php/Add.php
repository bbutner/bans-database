<?php

//Server Information
$servername = "localhost";
$dbusername = "bansdbuser";
$dbpassword = "REDACTED";
$dbname = "bansdb";

//Ban Information
$admin_username = $_POST['ADMIN_USERNAME'];
$username = $_POST['USERNAME'];
$guid = $_POST['GUID'];
$ip = $_POST['IP'];
$ban_reason = $_POST['BAN_REASON'];

//Authentication to even attempt the connection
if ($_POST['PASSWORD'] != 'testing') {
    die("Invalid Credentials");
}

//Create the connection
$connection = new mysqli($servername, $dbusername, $dbpassword, $dbname);

//If the connection fails
if ($connection->connect_error) {
    die("Connection Failed: " . $connection->connect_error);
} else {
    echo "Connected";
}

//Write new ban to the MySQL Database
//$sql = "INSERT INTO bans (DATE, DBUSERNAME, GUID, IP, USERNAME, BAN_REASON)
//VALUES ('$guid', '$ip', '$username', '$ban_reason')";

$sql = "INSERT INTO bans (DATE, DBUSERNAME, GUID, IP, USERNAME, BAN_REASON)
VALUES (?, ?, ?, ?, ?, ?)";

if ($stmt = mysqli_prepare($connection, $sql)) {
    mysqli_stmt_bind_param($stmt, 'ssssss', date("m-d"), $admin_username, $guid, $ip, $username, $ban_reason);
    if (mysqli_stmt_execute($stmt)) {
        echo "Ban successfully added.";
    } else {
        echo "Execute Error: " . $sql . mysqli_error($connection);
    }
} else {
    echo "Prepare Error: " . $sql . mysqli_error($connection);
}

//Closes the connection
mysqli_close($connection);

$file = file_put_contents("Creation Logs.txt", '||| ' . date("g:i/d-m") . ' ' . $admin_username . ' ' . $guid . ' ' . $ip . ' ' . $username . ' ' . $ban_reason . ' ' . '||| from ' . $_SERVER['REMOTE_ADDR'] . "\r\n", FILE_APPEND);