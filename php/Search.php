<?php
//Server Information
$servername = "localhost";
$dbusername = "bansdbuser";
$password = "REDACTED";
$dbname = "bansdb";

//Query Information
$guid = $_POST['GUID'];
$username = $_POST['USERNAME'];
$access_username = $_POST['ACCESS_USERNAME'];
$admin_username = $_POST['ADMIN_USERNAME'];
$ban_reason  = $_POST['BAN_REASON'];
$ip = $_POST['IP'];

//Authentication to even attempt the connection
if ($_POST['PASSWORD'] != 'testing') {
    die("Invalid Credentials!");
}

//Create Connection
$connection = mysqli_connect($servername, $dbusername, $password, $dbname);

//Check the Connection
if ($connection->connect_error){
    die("Connection failed: " . $connection->connect_error);
}

$sql = "SELECT CONCAT('~',DATE,'~',DBUSERNAME,'~',USERNAME,'~',GUID,'~',IP,'~',BAN_REASON,'@') FROM bans WHERE";

$types = json_decode($_POST['QUERY_TYPE'], true);

$searchTerms = '';

if (in_array("query_admin_username", $types)) {
    $sql = $sql . " DBUSERNAME LIKE " . "\"%" . $admin_username . "%\"" . " &&";
    $searchTerms = $searchTerms . $admin_username . ' ';
}

if (in_array("query_guid", $types)) {
    $sql = $sql . " GUID LIKE " . "\"%". $guid . "%\"" . " &&";
    $searchTerms = $searchTerms . $guid . ' ';
}

if (in_array("query_ip", $types)) {
    $sql = $sql . " IP LIKE " . "\"%" . $ip . "%\"" . " &&";
    $searchTerms = $searchTerms . $ip . ' ';
}

if (in_array("query_username", $types)) {
    $sql = $sql . " USERNAME LIKE " . "\"%" . $username . "%\"" . " &&";
    $searchTerms = $searchTerms . $username . ' ';
}

if (in_array("query_ban_reason", $types)) {
    $sql = $sql . " BAN_REASON LIKE " . "\"%" . $ban_reason . "%\"" . " &&";
    $searchTerms = $searchTerms . $ban_reason . ' ';
}

$sql_query = substr($sql, 0, -3);

$result = $connection->query($sql_query);

if (!$result) {
    die("Invalid Query: " . mysqli_error());
}

$rowCount = mysqli_num_rows($result);

$bans = [];

while ($row = $result->fetch_row()) {
    array_push($bans, $row[0]);
}

for ($x = 0; $x < count($bans); $x++) {
    echo($bans[$x]);
}

mysqli_close($connection);

$file = file_put_contents("Query Logs.txt", date("g:i/d-m") . ' Admin: ' . $access_username . ' searched for:  ' . $searchTerms . 'from ' . $_SERVER['REMOTE_ADDR'] . "\r\n", FILE_APPEND);

?>