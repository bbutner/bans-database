<?php
$servername = "localhost";
$dbusername = "bansdbuser";
$password = "REDACTED";
$dbname = "bansdb";

//Authentication to even attempt the connection
if ($_POST['PASSWORD'] != 'testing') {
    die("incorrect");
}

$connection = mysqli_connect($servername, $dbusername, $password, $dbname);

//if ($connection->connect_error){
//    die("Connection failed: " . $connection->connect_error);
//}

$sql = "SELECT * FROM bans";

$result = $connection->query($sql);

if (!$result) {
    die("Invalid Query: " . mysqli_error());
}

$rowCount = mysqli_num_rows($result);

echo($rowCount);

mysqli_close($connection);